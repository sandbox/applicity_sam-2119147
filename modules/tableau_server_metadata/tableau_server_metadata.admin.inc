<?php

/**
 * @file
 * A file to hold the admin functions for the tableau_server_metadata module
 */

/**
 * We have our own abstract class to allow us to get access to the database info
 */
abstract class TableauDatabase extends Database {

  /**
   * Method databaseOptions().
   */
  public static function databaseOptions() {
    $options = array();
    foreach (self::$databaseInfo as $key => $data) {
      $options[$key] = $key;
    }

    return $options;
  }

}

/**
 * Function tableau_server_metadata_admin_setting().
 */
function tableau_server_metadata_admin_settings($form1, $form_state) {
  $form = array();

  $wrapper_id = "tableau-server-metadata-admin-form";
  $form['#prefix'] = '<div id="' . $wrapper_id . '">';
  $form['#suffix'] = '</div>';

  $connections = TableauDatabase::databaseOptions();
  $form['tableau_server_metadata_connection'] = array(
    '#type' => 'select',
    '#title' => t('Database connection'),
    '#description' => t('Select a connection or fill out all of the paramters'),
    '#size' => 1,
    '#default_value' => variable_get('tableau_server_metadata_connection', ''),
    '#ajax' => array(
      'wrapper' => $wrapper_id,
      'callback' => 'tableau_server_metadata_admin_settings_callback',
    ),
    '#empty_value' => '',
    '#empty_option' => t('Select a connection'),
    '#options' => $connections,
  );

  // Get the value of the connection.
  if (isset($form_state['values']['tableau_server_metadata_connection'])) {
    $connection = $form_state['values']['tableau_server_metadata_connection'];
  }
  else {
    $connection = variable_get('tableau_server_metadata_connection', '');
  }

  if (empty($connection)) {
    $form['tableau_server_metadata_host'] = array(
      '#type' => 'textfield',
      '#title' => t('Database Host'),
      '#default_value' => variable_get('tableau_server_metadata_host', ''),
      '#size' => 60,
      '#maxlength' => 100,
      '#description' => t("The name of the server that will provide the metadata."),
      '#required' => TRUE,
    );

    $form['tableau_server_metadata_port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#default_value' => variable_get('tableau_server_metadata_port', '8060'),
      '#size' => 60,
      '#maxlength' => 100,
      '#description' => t("The port of the database on the server that will provide the metadata."),
      '#required' => TRUE,
    );

    $form['tableau_server_metadata_database'] = array(
      '#type' => 'textfield',
      '#title' => t('Database Name'),
      '#default_value' => variable_get('tableau_server_metadata_port', 'workgroup'),
      '#size' => 60,
      '#maxlength' => 100,
      '#description' => t("The port of the database on the server that will provide the metadata."),
      '#required' => TRUE,
    );

    $form['tableau_server_metadata_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Database User'),
      '#default_value' => variable_get('tableau_server_metadata_user', 'tblwgadmin'),
      '#size' => 60,
      '#maxlength' => 100,
      '#description' => t("The user name for metadata lookup."),
    );

    $form['tableau_server_metadata_pass'] = array(
      '#type' => 'password',
      '#title' => t('Database Password'),
      '#default_value' => variable_get('tableau_server_metadata_pass', ''),
      '#size' => 60,
      '#maxlength' => 100,
      '#description' => t("The password for metadata lookup."),
    );
  }

  return system_settings_form($form);
}

/**
 * Callback for the admin form.
 */
function tableau_server_metadata_admin_settings_callback($form, $form_state) {
  return $form;
}
