<?php
/**
 * @file
 * tableau_server_trusted.admin.inc
 *
 * The administration functions for using Tableau Server in trusted
 * authentication mode.
 */

/**
 * Function tableau_server_metadata_admin_setting().
 */
function tableau_server_trusted_admin_settings($form1, $form_state) {
  $form = array();

  $form['tableau_server_trusted_ticket_host'] = array(
    '#type' => 'textfield',
    '#title' => t('The host name of the tableau server for trusted tickets'),
    '#default_value' => variable_get('tableau_server_trusted_ticket_host', ''),
    '#size' => 60,
    '#maxlength' => 100,
    '#description' => t("The name of the server that will provide the tickets without the HTTP:// part."),
    '#required' => TRUE,
  );

  $form['tableau_server_trusted_image_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Image User'),
    '#default_value' => variable_get('tableau_server_trusted_image_user', TABLEAU_SERVER_TRUSTED_IMAGE_USER),
    '#size' => 60,
    '#maxlength' => 100,
    '#description' => t("The user name to use when rendering images of the reports."),
    '#required' => TRUE,
  );

  $form['tableau_server_trusted_single_report_user'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Single Report User'),
    '#default_value' => variable_get('tableau_server_trusted_single_report_user', TABLEAU_SERVER_TRUSTED_REPORT_SINGLE_USER),
    '#description' => t("Should we use a single tableau user as the user name to use when embedding reports."),
    '#required' => TRUE,

  );

  $form['tableau_server_trusted_report_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Single Report Username'),
    '#default_value' => variable_get('tableau_server_trusted_report_user', TABLEAU_SERVER_TRUSTED_REPORT_USER),
    '#size' => 60,
    '#maxlength' => 100,
    '#description' => t("The user name to use when embedding reports."),
    '#required' => TRUE,
  );

  $form['tableau_server_trusted_detailed_debugging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Detailed debugging to watchdog'),
    '#default_value' => variable_get('tableau_server_trusted_detailed_debugging', false),
    '#description' => t("Do you want detailed debugging writtend to the Drupal log to aid with setup."),
  );

  return system_settings_form($form);
}
