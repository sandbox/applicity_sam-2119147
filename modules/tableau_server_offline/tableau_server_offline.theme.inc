<?php

/**
 * @file
 * tableau_server_offline.theme.inc
 *
 * Provides a theme function for displaying the report in an offline mode.
 */

/**
 * Function theme_tableau_server_report_js__offline().
 */
function theme_tableau_server_report_js__offline($vars) {

  $output = ''
    . '<div class="tableauPlaceholder" style="width:1004px; height:862px;">';

  $output .= "<h2>Offline Report Mode</h2>";
  $output .= "<pre>" .
      '&lt;object class="tableauViz" width="100%" height="100%" style="display:none;"&gt;'
    . '&lt;param name="host_url" value="' . $vars['host_url'] . '" /&gt;'
    . '&lt;param name="site_root" value="" /&gt;&lt;param name="name" value="' . $vars['name']
    . '" /&gt;&lt;param name="tabs" value="no" /&gt;&lt;param name="toolbar" value="no" /&gt;';

  // If we have a ticket then add the parameter.
  if (!empty($vars['ticket'])) {
    $output .= '&lt;param name="ticket" value="' . $vars['ticket'] . '" /&gt;';
  }

  $output .= '&lt;param name="load-order" value="' . $vars['load-order'] . '" /&gt;'
    . '&lt;/object&gt;</pre></div>';

  return $output;
}
