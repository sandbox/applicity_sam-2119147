<?php

/**
 * @file
 * An example migration class to transfer reports
 */

/**
 * TableauReportMigration class definition.
 */
class TableauReportMigration extends Migration {

  /**
   * Constructor.
   */
  public function __construct($arguments) {
    parent::__construct(MigrateGroup::getInstance('tableau', array('default')));

    $this->map = new MigrateSQLMap($this->machineName, array(
      'id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      ), MigrateDestinationNode::getKeySchema()
    );

    // This is only an example so you would need to create the content type.
    $this->destination = new MigrateDestinationNode('report_tableau');

    // Get the connection to use for the data.
    $connection = tableau_server_metadata_get_connection();
      $query = Database::getConnection('default', $connection)
      ->select('views', 'v');
    $query->join('sites', 's', 's.id = v.site_id');
    $query->join('workbooks', 'w', 'w.id = v.workbook_id');

    $query->fields('v', array('id', 'name', 'repository_url', 'created_at',
      'description', 'updated_at', 'title',
      'sheet_id', 'sheettype'));
    $query->addField('s', 'name', 'site_name');
    $query->addField('w', 'name', 'workbook_name');
    $this->source = new MigrateSourceSQL($query, array(), NULL, array('map_joinable' => FALSE));

    $this->addFieldMapping('title', 'title');
    $this->addFieldMapping('body', 'description');
    $this->addFieldMapping('created', 'created_at');
    $this->addFieldMapping('changed', 'updated_at');
    $this->addFieldMapping('field_tableau_report:site', 'site_name');
    $this->addFieldMapping('field_tableau_report:workbook', 'workbook_name');
    $this->addFieldMapping('field_tableau_report:view', 'sheet_id');

    $this->addUnmigratedSources(array('name', 'repository_url'));
  }

  /**
   * Method prepareRow($row).
   *
   * Sometimes used to manipulate the data on transfer.
   */
  public function prepareRow($row) {
    // dpm($row, 'Row');
    parent::prepareRow($row);
  }

}
