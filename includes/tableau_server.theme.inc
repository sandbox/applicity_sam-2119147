<?php

/**
 * @file
 * Theme functions for the tableau server module
 */

/**
 * Function theme_tableau_server_report.
 *
 * Render a tableau report in an iframe
 */
function theme_tableau_server_report($vars) {

  $site = $vars['site'];
  $workbook = $vars['workbook'];
  $view = $vars['view'];

  $markup = "Site:" . $site . "<br/>";
  $markup .= "Workbook:" . $workbook . "<br/>";
  $markup .= "View:" . $view . "<br/>";
  return $markup;
}

/**
 * Function tableau_server_preprocess_tableau_server_report_js.
 */
function tableau_server_preprocess_tableau_server_report_js(&$vars) {
  // Get the configured host name for the server.
  $host = variable_get('tableau_server_host', 'tableau');

  // Copy the parameters from the report definition.
  foreach (array('site', 'workbook', 'view') as $key) {
    if (empty($vars[$key]) && !empty($vars['report'][$key])) {
      $vars[$key] = $vars['report'][$key];
    }
  }

  $vars['host_url'] = 'http://' . $host . '/';

  // Set the load order.
  $vars['load-order'] = $vars['delta'] + 1;

  // Add the parameters for the report.
  $vars['site_root'] = '';
  if (!empty($vars['site']) && ($site = strtolower($vars['site'])) && $site <> 'default') {
    $vars['site_root'] = '/t/' . $site;
  }

  // Make the report name parameter.
  $name = tableau_server_make_view_path($vars['site'], $vars['workbook'], $vars['view']);
  $vars['view_path'] = $name;

  // Make sure that we encode the name.
  $vars['name'] = tableau_server_report_name_encode($name);

  // Build full URL of report view.
  $vars['url'] = 'http://' . $host . $vars['site_root'] . '/views/' . $vars['view_path'];
}

/**
 * Function theme_tableau_server_report_js().
 */
function theme_tableau_server_report_js($vars) {
  $settings = array(
    'url' => $vars['url'],
    'options' => array(
      'hideTabs' => TRUE,
      'hideToolbar' => TRUE,
      'width' => '100%',
    ),
  );
  if (!empty($vars['ticket'])) {
    $settings['options']['ticket'] = $vars['ticket'];
  }
  // Give other modules and themes the chance to alter the settings.
  drupal_alter('tableau_server_report_js_settings', $settings);
  drupal_add_js(array('tableauServer' => $settings), 'setting');

  // Make the url for the javascript api.
  $api_url = $vars['host_url'] . "javascripts/api/tableau_v8.js";
  drupal_add_js($api_url, 'external');
  drupal_add_js(drupal_get_path('module', 'tableau_server') . '/tableau_server.js');

  $output = '<div id="tableau-embed"></div>';
  return $output;
}

/**
 * Renders the tableau report as an image with the appropriate links.
 */
function theme_tableau_report_formatter_image($variables) {
  $item = $variables['item'];

  // Inside a view $variables may contain null data. In that case, just return.
  if (empty($item['fid']) || empty($item['imagefile'])) {
    return '';
  }

  $imagefile = $item['imagefile'];
  $title = isset($variables['entity']->title) ? $variables['entity']->title : '';

  $image = array(
    'path' => $imagefile->uri,
    'alt' => $title,
  );

  // Do not output an empty 'title' attribute.
  if (!empty($title)) {
    $image['title'] = $title;
  }

  if (!empty($variables['image_style'])) {
    $image['style_name'] = $variables['image_style'];
    $output = theme('image_style', $image);
  }
  else {
    $output = theme('image', $image);
  }

  if ($variables['path']) {
    $path = $variables['path']['path'];
    $options = $variables['path']['options'];
    // When displaying an image inside a link, the html option must be TRUE.
    $options['html'] = TRUE;
    $output = l($output, $path, $options);
  }

  return $output;
}
