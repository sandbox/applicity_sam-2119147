<?php

/**
 * @file
 * Provide a field handler for tableau report fields
 *
 * Primary value passed to this field must be the site for the report
 *
 * Subfields are used to specify all the other values:
 *   'workbook' - The workbook for the report.
 *   'view' - The view.
 *
 * @code
 *   // The site should be passed in as the primary value.
 *   $this->addFieldMapping('field_tableau_report', 'site');
 *   $this->addFieldMapping('field_tableau_report:workbook', 'workbook');
 *   $this->addFieldMapping('field_tableau_report:view', 'view');
 * @endcode
 */

class MigrateTableauReportFieldHandler extends MigrateImageFieldHandler {
  /**
   * Constructor.
   */
  public function __construct() {
    $this->registerTypes(array('tableau_report'));
  }

  /**
   * Provides the list of fields.
   */
  public function fields($type, $instance, $migration = NULL) {
    $fields = parent::fields($type, $instance, $migration);
    $fields += array(
      'site' => t('Site: The name of the site'),
      'workbook' => t('Workbook: The name of the workbook'),
      'view' => t('View: The name of the view'),
    );
    return $fields;
  }

  /**
   * Function prepare the field.
   */
  public function prepare($entity, array $field_info, array $instance, array $values) {

    // Let the file handler prepare the field
    $return = parent::prepare($entity, $field_info, $instance, $values);

    if (isset($values['arguments'])) {
      $arguments = $values['arguments'];
      unset($values['arguments']);
    }
    else {
      $arguments = array();
    }
    
    $language = $this->getFieldLanguage($entity, $field_info, $arguments);

    foreach (array('site', 'workbook', 'view') as $k) {
      $return[$language][0][$k] = $arguments[$k];
    }

    return isset($return) ? $return : NULL;
  }
}
