<?php

/**
 * @file
 * Admin pages for Tableau Server Module
 */

/**
 * Function tableau_server_admin_setting().
 */
function tableau_server_admin_settings() {
  $form = array();
  $form['tableau_server_host'] = array(
    '#type' => 'textfield',
    '#title' => t('The host name of the tableau server'),
    '#default_value' => variable_get('tableau_server_host', ''),
    '#size' => 60,
    '#maxlength' => 100,
    '#description' => t("The name of the server that will provide the reports without the HTTP:// part."),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}
