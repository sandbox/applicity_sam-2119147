(function ($) {
  Drupal.behaviors.tableauServer = {
    attach: function () {
      $("#tableau-embed").once('#tableau-server', function () {
        var container = this;
        var viz = new tableauSoftware.Viz(container, Drupal.settings.tableauServer.url, Drupal.settings.tableauServer.options);
        viz.addEventListener(tableauSoftware.TableauEventName.CUSTOM_VIEW_LOAD, function () {
          var iframe = container.getElementsByTagName("iframe")[0];
          var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
          var height = jQuery("#centeringContainer", innerDoc).height();
          if (Drupal.settings.tableauServer.css) {
            var link = jQuery('<link/>', innerDoc).attr('rel','stylesheet')
              .attr('type','text/css')
              .attr('href',Drupal.settings.tableauServer.css);
            jQuery('head', innerDoc).append(link);
          }
          // jQuery('#tableau-embed').show();
          iframe.style.height = height + "px";
        });
      });
    }
  };
}(jQuery));
