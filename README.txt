Tableau server
==============

A module to allow the user to embed a Tableau Server report into an entity.

It implements a field that stores the data required to present a report.
